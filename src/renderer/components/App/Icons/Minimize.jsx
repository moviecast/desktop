import React from 'react';

const Minimize = props => (
  <svg height="100%" width="100%" shapeRendering="crispEdges" {...props}>
    <line x1="17" y1="14" x2="27" y2="14" stroke="white" />
  </svg>
);

export default Minimize;
